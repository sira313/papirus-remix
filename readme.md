### More Flat

This a flat minimalist icon themes is based on [Papirus](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)

### Screenshot

![SS1](https://gitlab.com/sira313/more-flat/raw/master/SS/1.jpg)

### Install

```bash
cd ~/.local/share/icons
git clone https://gitlab.com/sira313/more-flat
```

Then set up the icons from the system settings

### Telegram System Tray

```bash
cd ~/.local/share/icons/more-flat/telegram-panel/tool/
chmod +x ticons.sh && ./ticons.sh
```

### To Do

- [x] Redesign `places`
- [ ] Redesign `apps`
- [ ] More colors
- [ ] Use png instead svg
  
### Support Me

[![](https://i.ibb.co/BKRhBpK/paypal-donate-button-high-quality-png.png)](https://paypal.me/aflasio)
