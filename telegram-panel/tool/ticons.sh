#!/bin/bash
set -e

cd ~/.local/share/TelegramDesktop/tdata/ticons
rm -r *

# no messages
cp ~/.local/share/icons/more-flat/telegram-panel/telegram.png ./icon_22.png

# muted, no messages
cp icon_22.png iconmute_22_0.png

# one message
cp ~/.local/share/icons/more-flat/telegram-panel/telegramnotification.png icon_22_1.png

# muted, one message
cp ~/.local/share/icons/more-flat/telegram-panel/telegrammute.png iconmute_22_1.png

# more messages
for f in {2..2000}; do ln -s icon_22_1.png icon_22_$f.png; done
for f in {2..2000}; do ln -s iconmute_22_1.png iconmute_22_$f.png; done 
